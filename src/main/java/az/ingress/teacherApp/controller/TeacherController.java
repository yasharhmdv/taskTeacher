package az.ingress.teacherApp.controller;

import az.ingress.teacherApp.service.TeacherService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/teacher")
public class TeacherController {

    private final TeacherService teacherService;

    public TeacherController(TeacherService teacherService) {
        this.teacherService = teacherService;
    }

    @RequestMapping
    public void create(@RequestBody Teacher teacher){
        teacherService.create(teacher);
    }
    @RequestMapping("{/id}")
    public void update(@RequestBody Teacher teacher,@PathVariable Integer id){
        teacherService.update(teacher, id);
    }

    @GetMapping
    public Teacher get(@PathVariable Integer id){
        return  teacherService.get(id);
    }
    @GetMapping
    public List<Teacher> getAll(){
        return teacherService.getAll();
    }
    @DeleteMapping
    public void delete(@PathVariable Integer id){
        teacherService.delete(id);
    }

}
