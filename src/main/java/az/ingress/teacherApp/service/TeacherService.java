package az.ingress.teacherApp.service;

import az.ingress.teacherApp.controller.Teacher;
import az.ingress.teacherApp.repository.TeacherRepository;

import java.util.List;

public class TeacherService {

    private final TeacherRepository teacherRepository;

    public TeacherService(TeacherRepository teacherRepository) {
        this.teacherRepository = teacherRepository;
    }

    public void create(Teacher teacher) {

        teacherRepository.save(teacher);
    }

    public void update(Teacher teacher, Integer id) {

        Teacher teacher1 = new Teacher();
        teacher1.setName(teacher.getName());
        teacher1.setId(teacher.getId());
        teacherRepository.save(teacher1);
    }

    public Teacher get(Integer id) {
        return teacherRepository.findById(id).get();
    }

    public List<Teacher> getAll() {
        return teacherRepository.findAll();
    }

    public void delete(Integer id) {

        teacherRepository.deleteById(id);
    }
}
