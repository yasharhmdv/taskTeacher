package az.ingress.teacherApp.repository;

import az.ingress.teacherApp.controller.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TeacherRepository extends JpaRepository<Teacher, Integer> {
}
